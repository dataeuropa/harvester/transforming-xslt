package io.piveau.transforming.xslt;

import io.piveau.pipe.PipeContext;
import io.piveau.transforming.repositories.RepositoryVerticle;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import org.ehcache.Cache;
import org.ehcache.CacheManager;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.CacheManagerBuilder;
import org.ehcache.config.builders.ExpiryPolicyBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.config.units.EntryUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.transform.*;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.UUID;

public class XsltTransformingVerticle extends AbstractVerticle {

    private final Logger log = LoggerFactory.getLogger(getClass());
    private final TransformerFactory transFact = TransformerFactory.newInstance();

    public static final String ADDRESS = "io.piveau.pipe.transformation.queue";

    private Cache<String, Future> cache;

    private final String instanceId = UUID.randomUUID().toString();

    @Override
    public void start(Promise<Void> startPromise) {
        vertx.eventBus().consumer(ADDRESS, this::handlePipe);

        CacheManager cacheManager = CacheManagerBuilder.newCacheManagerBuilder()
                .withCache("xslt-transformer-" + instanceId, CacheConfigurationBuilder.newCacheConfigurationBuilder(String.class, Future.class,
                                ResourcePoolsBuilder.newResourcePoolsBuilder().heap(100, EntryUnit.ENTRIES))
                        .withExpiry(ExpiryPolicyBuilder.timeToIdleExpiration(Duration.ofHours(2))))
                .build(true);

        cache = cacheManager.getCache("xslt-transformer-" + instanceId, String.class, Future.class);

        transFact.setURIResolver((href, base) -> {
            if (href.startsWith("http://europeandataportal.eu/edp-funct.xsl")) {
                return new StreamSource(new StringReader(vertx.fileSystem().readFileBlocking("edp-funct.xsl").toString()));
            }
            return null;
        });

        startPromise.complete();
    }

    private void handlePipe(Message<PipeContext> message) {
        PipeContext pipeContext = message.body();

        JsonObject config = pipeContext.getConfig();

        JsonObject dataInfo = pipeContext.getDataInfo();
        if (dataInfo.containsKey("content") && dataInfo.getString("content").equals("identifierList")) {
            pipeContext.log().trace("Passing pipe");
            pipeContext.pass();
            return;
        }

        String runId = pipeContext.getPipe().getHeader().getRunId();

        getOrCreateTransformer(runId, config)
                .onSuccess(transformer -> {
                    JsonObject info = pipeContext.getDataInfo();

                    try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
                        pipeContext.log().debug("Input: {}", pipeContext.getStringData());
                        transformer.transform(new StreamSource(new StringReader(pipeContext.getStringData())), new StreamResult(out));
                        String output = out.toString(StandardCharsets.UTF_8);
                        pipeContext.log().debug("Output: {}", output);

                        pipeContext.setResult(output, "application/rdf+xml", info).forward();
                        pipeContext.log().info("Data transformed: {}", info);
                    } catch (TransformerException | IOException e) {
                        log.error("Transforming data", e);
                        log.debug(pipeContext.getStringData());
                        pipeContext.log().error(info.toString(), e);
                    }
                })
                .onFailure(pipeContext::setFailure);
    }

    private Future<Transformer> getOrCreateTransformer(String runId, JsonObject config) {
        boolean useCache = config.getBoolean("single", true);
        if (useCache && cache.containsKey(runId)) {
            return (Future<Transformer>)cache.get(runId);
        } else {
            return Future.future(promise -> {
                if (useCache) {
                    cache.put(runId, promise.future());
                }
                getScript(config)
                        .onSuccess(script -> {
                            try {
                                final Transformer transformer = transFact.newTransformer(new StreamSource(new ByteArrayInputStream(script.getBytes(StandardCharsets.UTF_8))));
                                if (config.containsKey("params")) {
                                    config.getJsonObject("params").forEach(e -> transformer.setParameter(e.getKey(), e.getValue()));
                                }

                                promise.complete(transformer);
                            } catch (TransformerConfigurationException e) {
                                promise.fail(e);
                            }
                        })
                        .onFailure(promise::fail);
            });
        }
    }

    private Future<String> getScript(JsonObject config) {
        return Future.future(promise -> {
            switch (config.getString("scriptType")) {
                case "repository" -> {
                    JsonObject repository = config.getJsonObject("repository");
                    vertx.eventBus().<String>request(RepositoryVerticle.ADDRESS, repository)
                            .onSuccess(reply -> promise.complete(reply.body()))
                            .onFailure(promise::fail);
                }
                case "localFile" -> {
                    String path = "scripts/" + config.getString("path");
                    vertx.fileSystem().exists(path)
                            .compose(exists -> {
                                if (exists) {
                                    return vertx.fileSystem().readFile(path).map(Buffer::toString);
                                } else {
                                    return Future.failedFuture("Script " + path + " not found");
                                }
                            }).onComplete(promise);
                }
//                case "embedded": ->
                default -> promise.complete(config.getString("script"));
            }

        });
    }

}
