# Pipe Segment Configuration

The transformer can be configured through the config object as part of the segment body of the pipe.
It allows the configuration of either embedding the script directly in the config object or passing a references to a git repository:

```json
{
  "scriptType": "",
  "repository": {
    "uri": "",
    "branch": "",
    "username": "",
    "token": "",
    "script": ""
  },
  "script": ""
}
```
If `scriptType` is either `embedded`, which means the script is contained in the `script` field. Or has the value `repository` which indicates a script in a git repository described in more details in the `repository` field.

# Data Info Object

The data info object is just passed as is.

