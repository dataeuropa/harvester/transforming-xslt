# ChangeLog

## Unreleased

## 1.1.1 (2023-11-13)

**Changed:**
* Log output pattern including date and time
* Event loop for transforming verticle
* One instance for transforming verticle

**Fixed:**
* Interaction between cache and future improved
* `localFile` future completed twice due to missing break in switch 

## 1.1.0 (2022-04-23)

**Added:**
* Repository verticle to avoid conflicting git repo actions

**Changed:**
* Separate cache for each verticle instance
* Better asynchronous processing of pipes

## 1.0.2 (2021-10-20)

**Changed:**
* Important connector lib update

## 1.0.1 (2021-06-23)

**Changed:**
* Connector pipe handling

## 1.0.0 (2021-06-05)

**Added:**
* buildInfo.json for build info via `/health` path
* config.schema.json

**Changed:**
* Pass output as is (no json conversion) 
* Add `PIVEAU_` prefix to logstash configuration environment variables
* Update all dependencies

**Fixed:**
* Git repo handling

## 0.1.0 (2019-05-17)

**Added:**
* Environment variable for repository default branch

## 0.0.1 (2019-05-03)

Initial release
